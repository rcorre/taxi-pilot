extends KinematicBody

export var VELOCITY := 40.0
export var TARGET_RANGE := 5.0

var target: Spatial

func _process(delta):
	if not target:
		return

	var disp := target.global_transform.origin - global_transform.origin
	if disp.length() < TARGET_RANGE:
		var dir := disp.normalized()
		move_and_collide(dir * VELOCITY * delta)

func _on_DetectionArea_body_entered(body: Node):
	target = body as Spatial

func _on_DetectionArea_body_exited(body: Node):
	target = null
