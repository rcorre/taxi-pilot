extends CenterContainer

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_play_pressed():
	get_tree().change_scene("res://world/world.tscn")
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
