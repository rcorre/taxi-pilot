# The Last Taxi Pilot

In the future, all taxis are self-driving, except yours. Can you beat them?

Entry for [Open Jam 2019](https://itch.io/jam/open-jam-2019)

# Software Used:

- [Godot](https://godotengine.org/)
- [Blender](https://www.blender.org/)
- [Inkscape](https://inkscape.org/)
- [Audacity](https://www.audacityteam.org/)
- [Archlinux](https://www.archlinux.org/)
- [Neovim](https://neovim.io/)

# License

All code is licensed under the [MIT License](https://opensource.org/licenses/MIT).
Unless mentioned below, all assets are licensed under [CC-BY-3.0](https://creativecommons.org/licenses/by/3.0/).

# Credits

## Chiptone

The following sounds were created with [chiptone](http://sfbgames.com/chiptone/)
and are [CC0-licensed](https://creativecommons.org/publicdomain/zero/1.0/):

- pickup.wav
- dropoff.wav

## Fonts

This game uses the [Orbitron](https://www.theleagueofmoveabletype.com/orbitron) font,
licensed under the [Open Font License](./common/Open Font License.markdown).

## Game Icons

This game uses several icons from [game-icons.net](https://game-icons.net):

- https://game-icons.net/1x1/delapouite/path-distance.html (modified) [CC-BY-3.0](https://creativecommons.org/licenses/by/3.0/)
- https://game-icons.net/1x1/delapouite/person.html [CC-BY-3.0](https://creativecommons.org/licenses/by/3.0/)
- https://game-icons.net/1x1/lorc/spanner.html [CC-BY-3.0](https://creativecommons.org/licenses/by/3.0/)
- https://game-icons.net/1x1/sbed/battery-pack-alt.html [CC-BY-3.0](https://creativecommons.org/licenses/by/3.0/)

## Freesound

The following sounds from freesound were used:

- https://freesound.org/people/Metzik/sounds/377283/ [CC-BY-3.0](https://creativecommons.org/licenses/by/3.0/) (modified)
- https://freesound.org/people/Iwiploppenisse/sounds/156031/ [CC-BY-3.0](https://creativecommons.org/licenses/by/3.0/) (modified)
