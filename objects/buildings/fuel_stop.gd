extends Spatial

onready var refuel_sound := $RefuelSound as AudioStreamPlayer

export var fuel_rate := 25.0
export var fuel_cost := 0.4

var player: Node

func _on_body_entered(body: Node):
	print('entered')
	player = body

func _on_body_exited(body: Node):
	player = null

func _physics_process(delta):
	var refueling := false
	if player:
		var amount := min(fuel_rate * delta, player.max_fuel - player.fuel)
		var cost := amount * fuel_cost
		if amount > 0 and player.money > cost:
			player.refuel(amount, cost)
			refueling = true
			refuel_sound.pitch_scale = 1.0 + player.fuel / player.max_fuel
	if refuel_sound.playing != refueling:
		refuel_sound.playing = refueling
