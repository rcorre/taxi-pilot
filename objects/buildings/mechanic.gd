extends Spatial

onready var repair_sound := $RepairSound as AudioStreamPlayer

export var repair_rate := 5.0
export var repair_cost := 1.0

var player: Node

func _on_body_entered(body: Node):
	print('entered')
	player = body

func _on_body_exited(body: Node):
	print('exited')
	player = null

func _physics_process(delta):
	var repairing := false
	if player:
		var amount := min(repair_rate * delta, player.max_hull - player.hull)
		var cost := amount * repair_cost
		if amount > 0 and player.money > cost:
			player.repair(amount, cost)
			repairing = true
	if repair_sound.playing != repairing:
		repair_sound.playing = repairing
