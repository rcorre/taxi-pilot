extends Control

export(NodePath) var people_label
export(NodePath) var money_label
export(NodePath) var distance_label

func set_data(people: int, money: int, distance: float):
	get_node(people_label).set_text(str(people))
	get_node(money_label).set_text(str(money))
	get_node(distance_label).set_text("%.2f" % distance)
