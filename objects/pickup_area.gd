extends Area

onready var player = get_tree().get_nodes_in_group("player")[0]
onready var indicator = $Indicator/Viewport/Control

var people := []
var is_destination := false

func _on_body_entered(body: Node):
	if body == player:
		body.drop_passengers(self)
		is_destination = false
		while people:
			var p = people[0]
			if not body.add_passenger(p):
				break
			people.pop_front()
		# $Mesh.visible = len(people) > 0
		$DestinationArrow.visible = is_destination
		monitoring = len(people) > 0 or is_destination
		update_indicator()

func add_person(p: Dictionary):
	people.append(p)
	# $Mesh.visible = true
	monitoring = true
	update_indicator()

func add_destination():
	$DestinationArrow.visible = true
	is_destination = true
	monitoring = true

func update_indicator():
	indicator.visible = len(people) > 0
	var total_money := 0.0
	var total_distance := 0.0
	for p in people:
		total_money += p.money
		total_distance += p.distance
	indicator.set_data(len(people), total_money, total_distance)
