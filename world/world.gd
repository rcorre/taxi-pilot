extends Spatial

onready var pickup_areas = get_tree().get_nodes_in_group("pickup_area")

export(int, 0, 100) var max_people := 10

func _ready():
	randomize()

func update_routes():
	var total_people := 0
	for a in pickup_areas:
		total_people += len(a.people)

	if total_people >= max_people:
		return

	var start_idx := randi() % len(pickup_areas)
	var end_idx := randi() % len(pickup_areas)

	if start_idx != end_idx:
		var start = pickup_areas[start_idx]
		var dest = pickup_areas[end_idx]
		start.add_person({
			"dest": dest,
			"money": randf() * 100.0,
			"distance": (
				dest.global_transform.origin -
				start.global_transform.origin
			).length()
		})
