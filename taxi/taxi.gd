extends KinematicBody

const CAMERA_ZOOM_FACTOR := 20.0
const CAMERA_MOVE_FACTOR := 150.0
const MAX_ROLL := PI / 4
const ROLL_SPEED := 2.0
const MIN_CRASH_SPEED := 25.0
const MOUSE_SENSITIVITY := Vector2(0.05, 0.1)

export(float) var base_speed := 0.75
export(float) var max_speed := 1.5
export(float) var driftiness := 1.0
export(float) var pitch_factor := 2.0
export(float) var yaw_factor := 2.0
export(float) var acceleration := 0.5
export(float) var passenger_capacity := 4
export(float) var max_fuel := 50.0
export(float) var max_hull := 20.0
export(float) var fuel_consumption := 1.0
export(float) var crash_damage_factor := 0.2

onready var hud := $HUD as Control
onready var camera := $Camera as Camera
onready var speed_animator := $SpeedAnimator as AnimationPlayer

var passengers := []
var money := 0.0
var fuel := max_fuel
var hull := max_hull
var pitch_target := 0.0
var yaw_target := 0.0
var drift := 0.0
var velocity := Vector3()

func _ready():
	hud.set_passenger_capacity(passenger_capacity)
	connect("body_entered", self, "_on_body_entered")

func _on_body_entered(body: Node):
	var speed := velocity.length()
	var dmg := max((speed - MIN_CRASH_SPEED) * crash_damage_factor, 0)
	hull -= dmg
	if hull <= 0:
		$AnimationPlayer.play("explode")
		hud.game_over(money)

func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		yaw_target -= event.relative.x * MOUSE_SENSITIVITY.x
		pitch_target -= event.relative.y * MOUSE_SENSITIVITY.y

func _physics_process(delta: float):
	# pitch, yaw, roll
	var pitch := ((
		Input.get_action_strength("pitch_up") -
		Input.get_action_strength("pitch_down")
	) + pitch_target) * pitch_factor
	var yaw := ((
		Input.get_action_strength("yaw_left") -
		Input.get_action_strength("yaw_right")
	) + yaw_target) * yaw_factor
	var throttle := Input.get_action_strength("accelerate")
	var brake := Input.get_action_strength("brake")

	yaw_target = 0.0
	pitch_target = 0.0

	if throttle == 0:
		throttle = -brake

	if fuel <= 0:
		throttle = min(throttle, 0)

	var fwd = global_transform.basis.z.normalized()

	var roll_target := -yaw / yaw_factor * MAX_ROLL
	rotate(Vector3.UP, yaw * delta)
	rotate(global_transform.basis.x, pitch * delta)
	rotate(global_transform.basis.z, (roll_target - rotation.z) * ROLL_SPEED * delta)

	var target_speed := base_speed + (max_speed - base_speed) * throttle
	var speed := velocity.length()
	speed = speed + (target_speed - speed) * acceleration * delta
	drift = clamp(drift + brake - delta, 0.0, 0.95)
	velocity = speed * (
		drift * velocity.normalized() +
		(1.0 - drift) * fwd
	)

	move_and_collide(velocity)

	camera.look_at(global_transform.origin + velocity * delta * CAMERA_MOVE_FACTOR, Vector3.UP)

	if throttle > 0:
		fuel -= delta * fuel_consumption

	hud.set_speed_factor(speed / base_speed)
	speed_animator.seek(speed / max_speed)

func _process(delta):
	hud.set_fuel(fuel, max_fuel)
	hud.set_hull(hull, max_hull)
	hud.set_money(money)

func add_passenger(p: Dictionary):
	if len(passengers) >= passenger_capacity:
		return false
	passengers.append(p)
	p.dest.add_destination()
	hud.pickup(len(passengers), passenger_capacity)
	return true

func drop_passengers(area: Area):
	var new_passengers := passengers.duplicate()
	for p in passengers:
		if p.dest == area:
			new_passengers.erase(p)
			hud.dropoff(len(new_passengers), passenger_capacity)
			money += p.money
	passengers = new_passengers

func refuel(amount: float, cost: float):
	fuel += amount
	money -= amount

func repair(amount: float, cost: float):
	hull += amount
	money -= amount
