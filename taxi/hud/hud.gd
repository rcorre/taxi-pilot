extends Control

const MINIMAP_ZOOM_FACTOR := 100.0

onready var money_label := $GridContainer/HBoxContainer/HBoxContainer/HBoxContainer/MoneyCount as Label
onready var passenger_label := $GridContainer/HBoxContainer/HBoxContainer/HBoxContainer/PassengerCount as Label
onready var minimap_camera := $ViewportContainer/Viewport/Camera as Camera
export(NodePath) var fuel_bar
export(NodePath) var hull_bar
export(NodePath) var game_over_label
export(NodePath) var profit_label

func set_passenger_capacity(capacity: int):
	passenger_label.set_text("0/%d" % capacity)

func pickup(passenger_count: int, max_count: int):
	$Sounds/Pickup.play()
	passenger_label.set_text("%d/%d" % [passenger_count, max_count])

func dropoff(passenger_count: int, max_count: int):
	$Sounds/Dropoff.play()
	passenger_label.set_text("%d/%d" % [passenger_count, max_count])

func set_money(val: int):
	money_label.set_text(str(val))

func set_speed_factor(val: int):
	minimap_camera.size = 100.0 + val * MINIMAP_ZOOM_FACTOR

func set_fuel(val: float, max_val: float):
	var bar := get_node(fuel_bar)
	bar.value = val
	bar.max_value = max_val

func set_hull(val: float, max_val: float):
	var bar := get_node(hull_bar)
	bar.value = val
	bar.max_value = max_val

func game_over(profit: float):
	get_node(game_over_label).visible = true
	get_node(profit_label).visible = true
	get_node(profit_label).set_text("Total profit: %.2f" % profit)
	yield(get_tree().create_timer(3), "timeout")
	get_tree().change_scene("res://gui/title_screen.tscn")
